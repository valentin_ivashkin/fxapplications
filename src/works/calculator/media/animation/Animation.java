package works.calculator.media.animation;

import javafx.animation.TranslateTransition;
import javafx.scene.Node;
import javafx.util.Duration;

public class Animation {
    private TranslateTransition transition;

    public Animation(Node node) {
        transition = new TranslateTransition(Duration.millis(20), node);
        transition.setFromX(-10);
        transition.setByX(10);
        transition.setCycleCount(3);
        transition.setAutoReverse(true);
    }

    public void playAnimation() {
        transition.playFromStart();
    }
}
