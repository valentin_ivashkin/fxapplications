package works.calculator;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import works.calculator.media.animation.Animation;


public class Controller {

    @FXML
    private TextField firstNumberField;

    @FXML
    private TextField secondNumberField;

    @FXML
    private Button buttonAdd;

    @FXML
    private Button buttonSub;

    @FXML
    private Button buttonMul;

    @FXML
    private Button buttonDiv;

    @FXML
    private Button buttonClr;

    @FXML
    private TextField resultField;

    @FXML
    void initialize() {
        buttonAdd.setOnAction(event -> add());
        buttonSub.setOnAction(event -> sub());
        buttonMul.setOnAction(event -> mul());
        buttonDiv.setOnAction(event -> div());
        buttonClr.setOnAction(event -> clr());
    }

    private void add() {
        if (isEmptyFields()) return;
        int[] numbers;
        if (!((numbers=getNumbers())==null)) {
            int result = numbers[0] + numbers[1];
            resultField.setText(String.valueOf(result));
        }
    }

    private void sub() {
        if (isEmptyFields()) return;
        int[] numbers;
        if (!((numbers=getNumbers())==null)) {
            int result = numbers[0] - numbers[1];
            resultField.setText(String.valueOf(result));
        }
    }


    private void mul() {
        if (isEmptyFields()) return;
        int[] numbers;
        if (!((numbers=getNumbers())==null)) {
            int result = numbers[0] * numbers[1];
            resultField.setText(String.valueOf(result));
        }
    }

    private void div() {
        if (isEmptyFields()) return;
        int[] numbers;
        if (!((numbers=getNumbers())==null)) {
            if (numbers[1] == 0) {
                resultField.setText("На ноль делить нельзя");
                return;
            }
            int result = numbers[0] / numbers[1];
            resultField.setText(String.valueOf(result));
        }
    }

    private int[] getNumbers() {
        try {
            int firstNum = Integer.parseInt(firstNumberField.getText());
            int secondNum = Integer.parseInt(secondNumberField.getText());
            return new int[]{firstNum, secondNum};
        }catch (Exception e){
            resultField.setText("Числа некорректны");
        }
        return null;
    }


    private boolean isEmptyFields() {
        if (firstNumberField.getText().isEmpty() || secondNumberField.getText().isEmpty()) {
            emptyFieldsAnimation();
            return true;
        }
        return false;
    }

    private void emptyFieldsAnimation() {
        resultField.setText("Вы не ввели числа");
        Animation firstNumFieldAnim = new Animation(firstNumberField);
        Animation secondNumFieldAnim = new Animation(secondNumberField);
        secondNumFieldAnim.playAnimation();
        firstNumFieldAnim.playAnimation();
    }

    private void clr() {
        firstNumberField.clear();
        secondNumberField.clear();
        resultField.clear();
    }
}
