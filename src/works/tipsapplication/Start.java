package works.tipsapplication;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Start extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("window.fxml"));
        primaryStage.getIcons().add(new Image("works\\tipsapplication\\images\\icon.jpg"));
        primaryStage.setResizable(false);
        primaryStage.setTitle("Расчет чаевых");
        primaryStage.setScene(new Scene(root, 557.0, 345.0));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}