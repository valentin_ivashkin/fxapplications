package works.tipsapplication;

import com.jfoenix.controls.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.fxml.FXML;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Класс для подсчета чаевых
 *
 * @author Walentin
 */
public class Controller {
    private double bill;
    private double total;
    private double tips;
    private double[] noRoundResults = new double[5];
    private final static double[] CURSES_OF_CURRENCIES = {1, 74.44, 80.71, 0.96, 0.68, 10.22}; //рубль, доллар, евро, рупий, иена и юань соответсвенно
    private ArrayList<Label> outputsLabels; // группа лейблов, куда выводятся результаты

    @FXML
    private TextField billInput;

    @FXML
    private Label billInRubLabel;

    @FXML
    private Label billInRubOutput;

    @FXML
    private JFXComboBox<String> currencySelectComboBox = new JFXComboBox<>();

    @FXML
    private JFXRadioButton tips10RadioButton;

    @FXML
    private JFXRadioButton tips5RadioButton;

    @FXML
    private JFXRadioButton noTipsRadioButton;

    @FXML
    private Label tipsRubLabel;

    @FXML
    private Label tipsOutput;

    @FXML
    private Label totalRubLabel;

    @FXML
    private JFXToggleButton roundSwitch;

    @FXML
    private JFXButton clearButton;

    @FXML
    private Label tipsRubCurOutput;

    @FXML
    private Label totalOutput;

    @FXML
    private Label totalRubCurOutput;

    @FXML
    private JFXToggleButton viewRubCurrencySumSwitch;

    @FXML
    void initialize() {
        loadListCurrencyComboBox();
        showHiddenRusOutputs();
        outputsLabels = getOutputsLabels();
        billInput.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> { // добавляем слушатель, который запускает процесс расчета чаевых
            checkBillInputNumber(newValue);
            calculateTips();
            showHiddenClrButton();
        });

        clearButton.setOnAction(event -> billInput.clear());

        tips10RadioButton.setOnAction(event -> calculateTips());
        tips5RadioButton.setOnAction(event -> calculateTips());
        noTipsRadioButton.setOnAction(event -> calculateTips());

        currencySelectComboBox.setOnAction(event -> {
            showHiddenRusOutputs();
            showHiddenRusCurSwitch();
            calculateTips();
        });
        viewRubCurrencySumSwitch.setOnAction(event -> showHiddenRusOutputs());
        roundSwitch.setOnAction(event -> {
            if (roundSwitch.isSelected()) roundToInt();
            else returnToDouble();
        });
    }

    private ArrayList<Label> getOutputsLabels() {
        return new ArrayList<>(Arrays.asList(tipsOutput, tipsRubCurOutput, totalOutput, totalRubCurOutput, billInRubOutput));
    }

    /**
     * Загружаем комбобокс с валютами
     */
    private void loadListCurrencyComboBox() {
        ObservableList<String> currencies = FXCollections.observableArrayList("Рубль", "Доллар", "Евро", "Рупий", "Иена", "Юань");
        currencySelectComboBox.setItems(currencies);
        currencySelectComboBox.setValue(currencies.get(1));
    }

    /**
     * Показывает или скрывает поля, куда выводятся результат в рублях
     */
    private void showHiddenRusOutputs() {
        if (currencySelectComboBox.getSelectionModel().getSelectedIndex() == 0 || !viewRubCurrencySumSwitch.isSelected()) {
            setOpacityRusOutput(0);
        } else {
            setOpacityRusOutput(1);
        }
    }

    private void setOpacityRusOutput(int valueOpacity) {
        billInRubLabel.setOpacity(valueOpacity);
        billInRubOutput.setOpacity(valueOpacity);
        tipsRubLabel.setOpacity(valueOpacity);
        tipsRubCurOutput.setOpacity(valueOpacity);
        totalRubCurOutput.setOpacity(valueOpacity);
        totalRubLabel.setOpacity(valueOpacity);
    }

    /**
     * Проверяет поле ввода на числа
     *
     * @param newValue новое значение, отличающееся от старого
     */
    private void checkBillInputNumber(String newValue) {
        if (!newValue.matches("^[0-9]{1,10}\\.?[0-9]{0,2}$|")) {
            billInput.setText(billInput.getText().substring(0, billInput.getText().length() - 1));
        } else setBillFromInput();
    }

    /**
     * Получаем введенную сумму из поля ввода
     */
    private void setBillFromInput() {
        if (isEmptyInput()) bill = Double.parseDouble(billInput.getText());
    }

    /**
     * Проверяет, не пустое ли поле ввода
     *
     * @return true - не пустое, false - пустое
     */
    private boolean isEmptyInput() {
        if (!billInput.getText().equals("")) {
            return true;
        }
        clearValueOutputs();
        return false;
    }

    /**
     * Очищает поля вывода результатов
     */
    private void clearValueOutputs() {
        totalOutput.setText("");
        totalRubCurOutput.setText("");
        tipsOutput.setText("");
        tipsRubCurOutput.setText("");
        billInRubOutput.setText("");
    }

    /**
     * Подсчет чаевых
     */
    private void calculateTips() {
        double tipsPercent = getTipsPercent();
        if (isEmptyInput()) {
            tips = bill * tipsPercent;
            total = tips + bill;
            outputSums(totalOutput, total);// общая сумма
            outputSums(tipsOutput, tips);// чаевые
            outputInOtherCurrencies();
            setBinaryFormat();
            roundIsSelectedRoundSwitch();
        }
    }

    /**
     * Выводит сумму в других валютах
     */
    private void outputInOtherCurrencies() {
        for (int i = 0; i < currencySelectComboBox.getItems().size(); i++) {
            if (i == currencySelectComboBox.getSelectionModel().getSelectedIndex()) {
                billInRubOutput.setText(String.valueOf(bill * CURSES_OF_CURRENCIES[i]));
                tipsRubCurOutput.setText(String.valueOf(tips * CURSES_OF_CURRENCIES[i]));
                totalRubCurOutput.setText(String.valueOf(total * CURSES_OF_CURRENCIES[i]));
            }
        }
    }

    /**
     * Преобразовывает в денежный формат
     */
    private void setBinaryFormat() {
        BigDecimal[] sums = new BigDecimal[outputsLabels.size()];
        if (isEmptyInput()) {
            for (int i = 0; i < outputsLabels.size(); i++) {
                sums[i] = BigDecimal.valueOf(Double.parseDouble(outputsLabels.get(i).getText()));
                sums[i] = sums[i].setScale(2, RoundingMode.CEILING);
                outputSums(outputsLabels.get(i), sums[i]);

            }
        }
    }

    private void roundIsSelectedRoundSwitch() {
        if (roundSwitch.isSelected()) roundToInt();
    }

    /**
     * Округляет до целого числа
     */
    private void roundToInt() {
        if (isEmptyInput()) {
            double[] sums = new double[outputsLabels.size()];
            for (int i = 0; i < outputsLabels.size(); i++) {
                sums[i] = Double.parseDouble(outputsLabels.get(i).getText());
                noRoundResults[i] = sums[i];
                sums[i] = Math.round(sums[i]);
                outputSums(outputsLabels.get(i), sums[i]);
                setBinaryFormat();
            }
        }
    }

    /**
     * Обращает округление до целого
     */
    private void returnToDouble() {
        for (int i = 0; i < outputsLabels.size(); i++) {
            outputsLabels.get(i).setText(String.valueOf(noRoundResults[i]));
        }
        setBinaryFormat(); // приведем восстановленные значения к двойному формату
    }

    /**
     * Выводи суммы в поля
     *
     * @param sumLabelOutput поле
     * @param sum            сумма
     */
    private void outputSums(Label sumLabelOutput, double sum) {
        sumLabelOutput.setText(String.valueOf(sum));
    }

    private void outputSums(Label sumLabelOutput, BigDecimal sum) {
        sumLabelOutput.setText(String.valueOf(sum));
    }

    /**
     * Скрывает или показывает кнопку вывода в рублях
     */
    private void showHiddenRusCurSwitch() {
        if (currencySelectComboBox.getSelectionModel().getSelectedIndex() == 0) {
            viewRubCurrencySumSwitch.setOpacity(0);
            viewRubCurrencySumSwitch.setDisable(true);
        } else {
            viewRubCurrencySumSwitch.setOpacity(1);
            viewRubCurrencySumSwitch.setDisable(false);
        }
    }

    /**
     * Получаем процент чаевых
     *
     * @return проценты
     */
    private double getTipsPercent() {
        if (noTipsRadioButton.isSelected()) return 0;
        if (tips5RadioButton.isSelected()) return 0.05;
        if (tips10RadioButton.isSelected()) return 0.1;
        return 0;
    }

    /**
     * Отображает кнопку очистить, если есть число и убирает ее, если числа нет
     */
    private void showHiddenClrButton() {
        if (!billInput.getText().equals("")) {
            clearButton.setOpacity(1);
            clearButton.setDisable(false);
        } else {
            clearButton.setOpacity(0);
            clearButton.setDisable(true);
        }
    }
}