package works.converter;

import java.util.ArrayList;
import java.util.Arrays;

class GetUnitMeasurements {
    static ArrayList<ArrayList<String>> getUnitMeasurementsList(){
        return new ArrayList<>(Arrays.asList(
                new ArrayList<>(Arrays.asList("км/ч", "мили", "м/c", "футы")),
                new ArrayList<>(Arrays.asList("кг", "гр", "т", "фунт")),
                new ArrayList<>(Arrays.asList("Па", "кгс/см2", "бар", "Торр")),
                new ArrayList<>(Arrays.asList("руб", "доллар", "евро", "МояВалюта"))
        ));
    }
}
