package works.converter;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.util.converter.NumberStringConverter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Controller {

    @FXML
    private Button clearButton;

    @FXML
    private TextField valueTextField1;
    private DoubleProperty valueField1Property = new SimpleDoubleProperty(0);

    @FXML
    private TextField valueTextField2;
    private DoubleProperty valueField2Property = new SimpleDoubleProperty(0);

    @FXML
    private ComboBox<String> convertValues = new ComboBox<>();

    @FXML
    private ComboBox<String> unitMeasurementComboBox1 = new ComboBox<>();

    @FXML
    private ComboBox<String> unitMeasurementComboBox2 = new ComboBox<>();
    private double[][][] tableOfCoefficients = GetCoefficients.getCoefficients(); //загружаем коэффициенты
    private ArrayList<ArrayList<String>> unitMeasurementsArray = GetUnitMeasurements.getUnitMeasurementsList(); //загружаем единицы измерения

    @FXML
    void initialize() {
        loadPhysicalValuesComboBox();
        loadUnitMeasurementComboBox(unitMeasurementComboBox1);
        loadUnitMeasurementComboBox(unitMeasurementComboBox2);
        valueTextField1.textProperty().bindBidirectional(valueField1Property, new NumberStringConverter());
        valueTextField2.textProperty().bindBidirectional(valueField2Property, new NumberStringConverter());
        convertValues.setOnAction(event -> {
            loadUnitMeasurementComboBox(unitMeasurementComboBox1);
            loadUnitMeasurementComboBox(unitMeasurementComboBox2);
        });
        unitMeasurementComboBox1.setOnAction(event -> convertPhysicalValues());
        unitMeasurementComboBox2.setOnAction(event -> convertPhysicalValues());

        valueField1Property.addListener((observable, oldValue, newValue) -> convertPhysicalValues());
        clearButton.setOnAction(event -> {
            valueTextField1.clear();
            valueTextField2.clear();
        });
    }

    private void loadUnitMeasurementComboBox(ComboBox<String> unitMeasurementComboBox) {
        for (int i = 0; i < unitMeasurementsArray.size(); i++) {
            if (convertValues.getSelectionModel().getSelectedItem().equals(convertValues.getItems().get(i))) {
                ObservableList<String> currency = FXCollections.observableArrayList(unitMeasurementsArray.get(i));
                unitMeasurementComboBox.setItems(currency);
                unitMeasurementComboBox.setValue(unitMeasurementsArray.get(i).get(0));
            }
        }
    }

    private void loadPhysicalValuesComboBox() {
        List<String> physValue = new ArrayList<>(Arrays.asList("Скорость", "Масса", "Давление", "Валюта"));
        ObservableList<String> currency = FXCollections.observableArrayList(physValue);
        convertValues.setItems(currency);
        convertValues.setValue(physValue.get(0));
    }

    private void convertPhysicalValues() {
        int outIndex = convertValues.getSelectionModel().getSelectedIndex();
        int inIndex = unitMeasurementComboBox1.getSelectionModel().getSelectedIndex();
        int coefficientIndex = unitMeasurementComboBox2.getSelectionModel().getSelectedIndex();
        if (outIndex != -1 & inIndex != -1 & coefficientIndex != -1) {
            double convertedValue = valueField1Property.get() * tableOfCoefficients[outIndex][inIndex][coefficientIndex];
            valueTextField2.setText(String.valueOf(convertedValue));
        }
    }
}

