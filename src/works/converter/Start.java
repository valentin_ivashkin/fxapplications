package works.converter;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Start extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("appView.fxml"));
        primaryStage.getIcons().add(new Image("works\\converter\\images\\icon.png"));
        primaryStage.setResizable(false);
        Scene scene = new Scene(root, 299.0, 269.0);
        scene.getStylesheets().add(getClass().getResource("stylesheets\\style.css").toExternalForm());
        primaryStage.setScene(scene);
        primaryStage.setTitle("Конвертер величин");
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
