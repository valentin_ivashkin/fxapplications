package works.addingmachine;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;


public class Controller {

    @FXML
    private TextField first_number_filed;

    @FXML
    private TextField second_number_field;

    @FXML
    private Button buttonAdd;

    @FXML
    private Button buttonClr;

    @FXML
    private TextField result_field;

    @FXML
    void initialize() {
        buttonAdd.setOnAction(event -> {
            int firstNum = Integer.parseInt(first_number_filed.getText());
            int secondNum = Integer.parseInt(second_number_field.getText());
            int result = firstNum + secondNum;
            result_field.setText(String.valueOf(result));
        });
        buttonClr.setOnAction(event -> {
            first_number_filed.clear();
            second_number_field.clear();
            result_field.clear();
        });

    }

}
