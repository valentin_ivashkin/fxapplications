package works.addingmachine;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("window.fxml"));
        primaryStage.getIcons().add(new Image("works\\addingmachine\\images\\icon.png"));
        primaryStage.setTitle("Арифмометр");
        primaryStage.setScene(new Scene(root, 459.0, 403.0));
        primaryStage.show();

    }


    public static void main(String[] args) {
        launch(args);
    }
}