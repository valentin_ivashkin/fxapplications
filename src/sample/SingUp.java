package sample;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;

public class  SingUp {

    @FXML
    private JFXTextField firstnameField;

    @FXML
    private JFXTextField locationField;

    @FXML
    private JFXTextField loginField;

    @FXML
    private JFXTextField lastnameField;

    @FXML
    private JFXCheckBox confirmCheckBox;

    @FXML
    private JFXButton singUpButton;

    @FXML
    private JFXComboBox<?> genderComboBox;

    @FXML
    private JFXPasswordField passwordField;

    @FXML
    private JFXButton prevWindowButton;

    @FXML
    void initialize(){
        confirmCheckBox.setOnAction(event -> {
            if (confirmCheckBox.isSelected()) singUpButton.setDisable(false);
            else singUpButton.setDisable(true);
        });

        prevWindowButton.setOnAction(event -> {
            prevWindowButton.getScene().getWindow().hide();
            LoaderOfScene.loadTheScene("singIn.fxml");
        });

        DBHandler dbHandler = new DBHandler();

        singUpButton.setOnAction(event -> {

            User user = new User (firstnameField.getText(), lastnameField.getText(), loginField.getText(),
                    passwordField.getText(), locationField.getText(),
                    genderComboBox.getSelectionModel().getSelectedItem().toString());
            dbHandler.singUpUser(user);

        });

    }

}
