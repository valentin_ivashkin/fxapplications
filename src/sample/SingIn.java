package sample;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SingIn {

    @FXML
    private JFXTextField loginField;

    @FXML
    private JFXButton singUpButton;

    @FXML
    private JFXPasswordField passwordField;

    @FXML
    private JFXButton singInButton;

    @FXML
    private JFXButton prevWindowButton;

    @FXML
    void initialize(){
        prevWindowButton.setOnAction(event -> {
            prevWindowButton.getScene().getWindow().hide();
            LoaderOfScene.loadTheScene("sample.fxml");

        });
        singUpButton.setOnAction(event -> {
            singUpButton.getScene().getWindow().hide();
            LoaderOfScene.loadTheScene("singUp.fxml");
        });

        singInButton.setOnAction(event -> {
            String loginText = loginField.getText().trim();
            String passwordText = passwordField.getText();

            if (!loginText.equals("") && !passwordText.equals("")) {
                loginUser(loginText, passwordText);
            } else System.out.println("Error");

        });

    }

    private void loginUser(String loginText, String passwordText) {
        DBHandler dbHandler = new DBHandler();
        User user = new User();
        user.setUsername(loginText);
        user.setPassword(passwordText);
        ResultSet resultSet = dbHandler.getUser(user);

        int counter = 0;

        while (true){
            try {
                if (!resultSet.next()) break;
            } catch (SQLException e) {
                e.printStackTrace();
            }
            counter++;

        }

        if (counter >= 1){
            System.out.println("Success!");

        }
    }


}
