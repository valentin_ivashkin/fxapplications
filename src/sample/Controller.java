package sample;

import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;

public class Controller {

    @FXML
    private JFXButton joinButton;
    @FXML
    void initialize(){
        joinButton.setOnAction(event -> {
            joinButton.getScene().getWindow().hide();
            LoaderOfScene.loadTheScene("singIn.fxml");

        });

    }

}

