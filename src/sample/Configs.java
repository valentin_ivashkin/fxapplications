package sample;
@SuppressWarnings("all")
public class Configs {
    protected String dbHost = "127.0.0.1";
    protected String dbPort = "3306";
    protected String dbUser = "root";
    protected String dbPass = "12345678";
    protected String dbName = "users";
    protected String dbParameters = "useUnicode=true&serverTimezone=UTC&useSSL=true&verifyServerCertificate=false";
}
