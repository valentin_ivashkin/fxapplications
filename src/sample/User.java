package sample;

class User {
    private String firstname;
    private String lastname;
    private String username;
    private String password;
    private String location;
    private String gender;

    User(String firstname, String lastname, String username, String password, String location, String gender) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.username = username;
        this.password = password;
        this.location = location;
        this.gender = gender;
    }

    public User() {

    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    String getFirstname() {
        return firstname;
    }

    String getLastname() {
        return lastname;
    }

    String getUsername() {
        return username;
    }

    String getPassword() {
        return password;
    }

    String getLocation() {
        return location;
    }

    String getGender() {
        return gender;
    }
}
